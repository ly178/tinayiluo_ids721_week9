import streamlit as st
from dotenv import load_dotenv
import os
import requests

# Load environment variables
load_dotenv()

# Set up the API token and headers for authentication
API_TOKEN = os.getenv("API_TOKEN")
HEADERS = {"Authorization": f"Bearer {API_TOKEN}"}
API_URL = "https://api-inference.huggingface.co/models/google/gemma-7b"


# Function to query the API with the user's symptoms
def query(symptoms):
    payload = {"inputs": symptoms}
    response = requests.post(API_URL, headers=HEADERS, json=payload)
    if response.status_code == 200:
        return response.json()
    else:
        return {"error": "Failed to fetch diagnosis"}


# Main function to run the Streamlit app
def main():
    st.title("Symptom Checker")
    st.write("Enter your symptoms to get a possible illness diagnosis.")

    # Text input for user to enter symptoms
    symptoms = st.text_input("Symptoms", placeholder="Describe your symptoms here")

    # Button to submit symptoms and get diagnosis
    if st.button("Get Diagnosis"):
        if symptoms:
            result = query(
                f"Give me the top illness that has symptoms of {symptoms}."
                " Just give me one sentence describing the illness."
            )
            # Check for errors in the response
            if "error" not in result:
                st.subheader("Possible Diagnosis:")
                # Assuming the API returns a string or a list of possible illnesses
                if isinstance(result, list):
                    # If the result is a list, display each item
                    for item in result:
                        st.write(item)
                else:
                    # If the result is a single item, display it
                    st.write(result)
            else:
                st.error("Failed to get a diagnosis. Please try again later.")
        else:
            st.warning("Please enter your symptoms before clicking on 'Get Diagnosis'.")


if __name__ == "__main__":
    main()
