import os
from dotenv import load_dotenv
import requests

load_dotenv()

API_TOKEN = os.getenv("API_TOKEN")


def test_huggingface_api():
    """Checks Hugging Face API with a prompt"""
    # Define headers with authorization token
    headers = {"Authorization": f"Bearer {API_TOKEN}"}

    # Define API URL for text generation model
    API_URL = "https://api-inference.huggingface.co/models/google/gemma-7b"

    # Test prompt
    test_prompt = "The answer to the universe is?"

    try:
        # Send POST request to model API
        response = requests.post(API_URL, headers=headers, json={"inputs": test_prompt})
        # Parse JSON response
        response_data = response.json()
        generated_text = response_data[0].get("generated_text")

        if generated_text is None:
            raise ValueError("Generated text is empty.")

        print("Hugging Face API test successful!")
        print("Generated text:", generated_text)

    except requests.exceptions.RequestException as e:
        print(f"Error occurred during Hugging Face API test: {e}")
    except ValueError as ve:
        print(f"Error in response data: {ve}")


if __name__ == "__main__":
    test_huggingface_api(API_TOKEN)
