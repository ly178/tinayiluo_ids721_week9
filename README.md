# tinayiluo_IDS721_week9

[![pipeline status](https://gitlab.com/ly178/tinayiluo_ids721_week9/badges/main/pipeline.svg)](https://gitlab.com/ly178/tinayiluo_ids721_week9/-/commits/main)

# Symptom Checker Web App

The Symptom Checker is a cutting-edge web application developed using Streamlit, integrating a sophisticated open-source Large Language Model (LLM) from Hugging Face. It's designed to interpret user-inputted symptoms and utilize AI to suggest possible diagnoses. This solution showcases the power of AI in healthcare, making preliminary medical advice more accessible. Deployed on AWS EC2, it stands as a testament to the scalability and robustness of cloud-based applications.

## Key Features

- **User Input Handling**: Users can effortlessly input their symptoms into a streamlined interface, making the application intuitive and easy to use for individuals of all ages.
- **Advanced Illness Identification**: By leveraging a state-of-the-art LLM from Hugging Face, the application analyses the symptoms and identifies potential illnesses, demonstrating the practical application of AI in diagnosing health conditions.
- **Immediate Diagnostic Output**: Users receive a concise diagnostic output, offering insights into possible illnesses based on their symptoms, thereby facilitating informed health decisions.

## Web App Delivery 

[Symptom Checker Web App](http://52.90.29.13:8501/)

Sample Demonstration:

![Screen_Shot_2024-04-06_at_8.38.54_PM](/uploads/5d68fad4883898861f3f1373aaa1e1fb/Screen_Shot_2024-04-06_at_8.38.54_PM.png)

## Project Requirements

To run and develop the Symptom Checker web app, you'll need the following:

- Streamlit
- Python 3.x
- Requests
- Dotenv
- Docker
- A valid API token from Hugging Face to access their open-source LLM.

## Project Structure Explained

- **`main.py`**: This is the heart of the application. It initializes the Streamlit web interface, manages user interactions, queries the Hugging Face API with symptoms, and displays possible diagnoses. It demonstrates effective use of API connections and UI elements in Streamlit.
  
- **`test_main.py`**: Contains unit tests to ensure the reliability of the application. It tests the connectivity with the Hugging Face API and the correctness of its responses, guaranteeing that the app behaves as expected under various conditions.
  
- **`Makefile`**: Offers a simplified way to execute repetitive tasks like installing dependencies, running tests, formatting code, and deploying the application. It's designed to streamline development workflows and ensure consistent code quality.
  
- **`gitlab-ci.yml`**: Configures the CI/CD pipeline in GitLab, automating the process of testing, linting, formatting, and deploying the application. This file ensures that every change goes through a rigorous quality check before deployment.
  
- **`Dockerfile`**: Contains the instructions to build a Docker container for the app, encapsulating the environment and dependencies needed to run it. This makes the app easily deployable and ensures consistency across different environments.
  
- **`requirements.txt`**: Lists all the Python packages that the app depends on. This file is crucial for reproducing the app's environment and ensuring that it runs smoothly on any machine.

## Installation and Deployment Guide

### Setting Up for Local Development

1. **Clone the Repository**: Start by cloning the repo to your local machine to get started with the app development.
2. **Install Dependencies**: Navigate to the project directory and execute `pip install -r requirements.txt` to install the necessary Python packages.
3. **Configure Environment Variables**: Create a `.env` file in the project root. Add your Hugging Face API token with the line `API_TOKEN=your_token_here` to authenticate API requests.
4. **Launch the Application**: Run `streamlit run main.py` and open `http://localhost:8501` in your browser to interact with the app locally.

### Running Tests

- Execute `make test` to run the test suite, validating both API connectivity and the application's response handling.

### Deploying on AWS EC2

1. **Prepare the AWS Environment**: Follow AWS documentation to set up an EC2 instance, allowing traffic on port 8501 and setting up security groups accordingly.
2. **Deploy**: SSH into your EC2 instance, clone the repository, and follow the environment setup instructions detailed earlier to get the app running on the cloud.
3. **Run in Background**: Utilize `screen` to run `streamlit run main.py` in the background, allowing the app to remain active after closing the SSH session.